-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versione server:              10.4.11-MariaDB - mariadb.org binary distribution
-- S.O. server:                  Win64
-- HeidiSQL Versione:            11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dump della struttura del database ictdb
CREATE DATABASE IF NOT EXISTS `ictdb` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `ictdb`;

-- Dump della struttura di tabella ictdb.loguser
CREATE TABLE IF NOT EXISTS `loguser` (
  `SESSION_ID` varchar(100) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`SESSION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dump dei dati della tabella ictdb.loguser: ~1 rows (circa)
/*!40000 ALTER TABLE `loguser` DISABLE KEYS */;
INSERT INTO `loguser` (`SESSION_ID`, `USER_ID`) VALUES
	('svaqlhemjelpk81jvqp340459u', 1);
/*!40000 ALTER TABLE `loguser` ENABLE KEYS */;

-- Dump della struttura di tabella ictdb.testa_risorse
CREATE TABLE IF NOT EXISTS `testa_risorse` (
  `ID` int(3) NOT NULL AUTO_INCREMENT,
  `TIPO` varchar(100) NOT NULL,
  `PROPRIETARIO` varchar(100) NOT NULL,
  `DESCRIZIONE` varchar(500) NOT NULL,
  `DETTAGLIO` char(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- Dump dei dati della tabella ictdb.testa_risorse: ~9 rows (circa)
/*!40000 ALTER TABLE `testa_risorse` DISABLE KEYS */;
INSERT INTO `testa_risorse` (`ID`, `TIPO`, `PROPRIETARIO`, `DESCRIZIONE`, `DETTAGLIO`) VALUES
	(1, 'libro', 'Prof X', 'Critica Ragione Pura', 'Dettaglio di Crtitica Ragione Pura'),
	(2, 'pc', 'Prof X', 'Lenovo', 'Dettaglio di Lenovo'),
	(3, 'appunti', 'Prof X', 'Matematica', 'Dettaglio di Appunti di Matematica'),
	(4, 'libro', 'Alunno F', 'Uno Nessuno Centomila', 'Dettaglio di Uno Nessuno Centomila'),
	(5, 'pc', 'Alunno F', 'MacBook', 'Dettaglio di MacBook'),
	(6, 'appunti', 'Alunno F', 'Lettere', 'Dettaglio di Lettere'),
	(7, 'libro', 'Biblioteca Y', 'Titoletto Libro', 'Dettaglio di Titoletto Libro'),
	(8, 'pc', 'Biblioteca Y', 'Desktop Prenotabile', 'Dettaglio di Desktop Prenotabile'),
	(9, 'appunti', 'Biblioteca Y', 'Filosofia 1900', 'Dettaglio di Filosofia 1900');
/*!40000 ALTER TABLE `testa_risorse` ENABLE KEYS */;

-- Dump della struttura di tabella ictdb.users
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(3) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(255) NOT NULL,
  `COGNOME` varchar(255) NOT NULL,
  `USERNAME` varchar(100) NOT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `USERNAME` (`USERNAME`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- Dump dei dati della tabella ictdb.users: ~3 rows (circa)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`ID`, `NOME`, `COGNOME`, `USERNAME`, `PASSWORD`) VALUES
	(1, 'ADMIN', 'ADMIN', 'ADMIN', '$2y$10$QVALCzY3o6w6iahl0Drg6OuDe1xRN0pn.OrMLzmNZC66RUlV9xYl2'),
	(2, 'Kevin', 'Corseto', 'FEMTO', '$2y$10$K.fC5bgXJGYsgPJfBAmFU.HLXXb1GqB8rpAG6Dq45UDiyHJeFiJyy'),
	(3, 'Federico', 'Demaria', 'Papu', '$2y$10$6BRWfJof.zSEYZs69Xdof.0hvpZSFZy9dk7ga8P0WcFxss1wUZPdW'),
	(8, 'Carlo', 'Pizza', 'Sgrodolo', '$2y$10$yjDyfsaPfHFfr0HbcLY4uufvDPUq.9A629DSoC8fc9Ado3xhXb.ne'),
	(9, 'Diana', 'Melinte', 'Duda', '$2y$10$UAFxcBl08wv3nqlMfKtisuLgrqQ25TzUPMuF5eEItq7eSI3pVpeQq'),
	(10, 'Franco', 'Roddi', 'Capitano', '$2y$10$Mj8Qznq9MR0bKJs2erppM.sHjrSAag56MHKxH2V0rcaaz3vlvyziC');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
