<?php
//classe per Registrazione
class AuthSys{
  private $PDO;
  public function __construct($PDOconn){ //costrutto per la creazione di connessione al passaggio del PDO su default.php
      $this->PDO = $PDOconn;
  }


public function registraNuovoUtente($post){
$input_user = trim($post['USERNAME']);
$input_nome = trim($post['NOME']);
$input_cognome = trim($post['COGNOME']);
$input_password = trim($post['PASSWORD']);
$input_confermaPass = trim($post['CONFERMA_PASSWORD']);

//controllo sulla Username (Solo caratteri alfa numerici e compreso tra 4 e 12 caratteri)
if(!(ctype_alnum($input_user) && mb_strlen($input_user) >= 4 && mb_strlen($input_user) <= 12)){
    throw new Exception("Username Non Valido");
}

//controllo unicità Username su DB
// try catch
$q = "SELECT * FROM users WHERE (USERNAME = :user)";
$rq = $this->PDO->prepare($q);
$rq->bindParam(":user", $input_user, PDO::PARAM_STR);
$rq->execute();
if($rq -> rowCount() > 0) {
  throw new Exception("USERNAME già presente");
}

//controllo NOME
if(mb_strlen($input_nome) == 0){
  throw new Exception("NOME non inserito");
  }

//controllo COGNOME
if(mb_strlen($input_cognome) == 0){
  throw new Exception("COGNOME non inserito");
  }

//controllo PASSWORD - caratteri permessi
if(!preg_match('/^[a-zA-Z0-9_\-\$#!?]{8,}$/', $input_password)){
  throw new Exception("Password non Valida, deve contenere minimo 8 Caratteri");
}

//controllo che PASSWORD e CONFERMA_PASSWORD coincidono
if(strcmp($input_password, $input_confermaPass) !== 0){
  throw new Exception(" La PASSWORD e La CONFERMA non coincidono, riprovare");
}

//Effettivo salvataggio du DB vvvvv

//hash della password (crypt)
$password_hash = password_hash($input_password, PASSWORD_DEFAULT);
//Inserimento
try {
  $q= "INSERT INTO users (NOME,COGNOME,USERNAME,PASSWORD) VALUES(:nome, :cognome, :user, :pwd)";
  $rq = $this->PDO->prepare($q);
  $rq->bindParam(":nome", $input_nome, PDO::PARAM_STR);
  $rq->bindParam(":cognome", $input_cognome, PDO::PARAM_STR);
  $rq->bindParam(":user", $input_user, PDO::PARAM_STR);
  $rq->bindParam(":pwd", $password_hash, PDO::PARAM_STR);
  $rq->execute();

if ($_SESSION['id'] !== '1') {
  header('location: ConfermaRegister.html');
}
else {
  header('location: UtentiGestione.php');
}
} catch (PDOException $e) {
  echo "Errore durante l'Inserimento Dati";
}

return TRUE;

}


//login
public function login(string $username, string $password){
  //verifica della Username tramite query su tabella users

  try {
    $q = "SELECT * FROM users WHERE USERNAME = :username";
    $rq = $this->PDO->prepare($q);
    $rq->bindParam(":username", $username, PDO::PARAM_STR);
    $rq->execute();
    if ($rq -> rowCount() == 0) {
        throw new Exception("ERRORE, i parametri iseriti non sono validi");
    }
    $record = $rq->fetch(PDO::FETCH_ASSOC); #se user è OK, restituisce un array con i dati del record per quell'utente
    //verifica PASSWORD
    if (!password_verify($password, $record['PASSWORD'])) {
      throw new Exception("Errore, Password errata");
    }


    $query_log = "SELECT * FROM loguser WHERE USER_ID = :id";
    $rqLog = $this->PDO->prepare($query_log);
    $rqLog->bindParam(":id", $record['ID'], PDO::PARAM_STR);
    $rqLog->execute();
    if ($rqLog -> rowCount() !== 0) {
        header('location: logout.php');

  } else {
    //inserimento dell'utente all'interno della tabaella loguser (sessione)

    $session_id = session_id();
    $user_id = $record['ID'];
    $q = 'INSERT INTO loguser (SESSION_ID, USER_ID) VALUES (:session, :userid)';
    $rq = $this->PDO->prepare($q);
    $rq->bindParam(":session", $session_id, PDO::PARAM_STR);
    $rq->bindParam(":userid", $user_id, PDO::PARAM_INT);
    $rq->execute();
    $_SESSION['id'] = $record['ID'];
    return TRUE;
}


      } catch (PDOException $e) {
    echo "Errore Accesso";
  }
}










public function logout(){
$id = $_SESSION['id'];
$query_destroy="DELETE from loguser where USER_ID = $id";
$result = $this->PDO->query($query_destroy);
unset($_SESSION["id"]);
header('location: login.php');
}

public function select(){

  $clauses=array();

  if(isset($_POST['search']))
  {

      if( isset( $_POST['f_tipo'] ) && !empty( $_POST['f_tipo'] ) ){
          $clauses[] = "TIPO LIKE '%{$_POST['f_tipo']}%'";
      }
      if( isset( $_POST['f_descrizione'] ) && !empty( $_POST['f_descrizione'] ) ){
          $clauses[] = "DESCRIZIONE LIKE '%{$_POST['f_descrizione']}%'";
      }
      if ( isset( $_POST['f_proprietario'] ) && !empty( $_POST['f_proprietario'] ) ){
          $clauses[]="PROPRIETARIO LIKE '%{$_POST['f_proprietario']}%'";
      }

      $where = !empty( $clauses ) ? ' where '.implode(' and ',$clauses ) : '';
      $sql = "SELECT ID, TIPO, DESCRIZIONE, PROPRIETARIO FROM testa_risorse " . $where;
      $result = $this->PDO->query($sql);
}
else {
  $sql = "SELECT ID, TIPO, DESCRIZIONE, PROPRIETARIO FROM testa_risorse ";
  $result = $this->PDO->query($sql);
}
      while($row=$result->fetch()){
        echo "<tr><td>".$row["TIPO"]."</td><td>".$row["DESCRIZIONE"]."</td><td>".$row["PROPRIETARIO"]."</td><td><button><a href=\"Dettaglio.php?id={$row['ID']}\">Dettaglio</a></button></td></tr>";
            }

      }

public function dettaglio(){
  $id=$_GET['id'];
  $sql = "SELECT ID, TIPO, DESCRIZIONE, PROPRIETARIO, DETTAGLIO FROM `testa_risorse` WHERE ID= $id " ;
  $result = $this->PDO->query($sql);

  while($row=$result->fetch()){
    echo "<tr><td>".$row["DESCRIZIONE"]."</td><td>".$row["PROPRIETARIO"]."</td><td>".$row["DETTAGLIO"]."</td></tr>";
        }
}


public function utentiAdmin(){
  $clauses=array();
  if(isset($_POST['search']))
  {

      if( isset( $_POST['f_user'] ) && !empty( $_POST['f_user'] ) ){
          $clauses[] = "USERNAME LIKE '%{$_POST['f_user']}%'";
      }
      if( isset( $_POST['f_nome'] ) && !empty( $_POST['f_nome'] ) ){
          $clauses[] = "NOME LIKE '%{$_POST['f_nome']}%'";
      }
      if ( isset( $_POST['f_cognome'] ) && !empty( $_POST['f_cognome'] ) ){
          $clauses[]="COGNOME LIKE '%{$_POST['f_cognome']}%'";
      }

      $where = !empty( $clauses ) ? ' where '.implode(' and ',$clauses ) : '';
      $sql = "SELECT ID, NOME, COGNOME, USERNAME, PASSWORD FROM users " . $where;
      $result = $this->PDO->query($sql);
}
else {
  $sql = "SELECT ID, NOME, COGNOME, USERNAME, PASSWORD FROM users";
  $result = $this->PDO->query($sql);
}
      while($row=$result->fetch()){
        echo "<tr><td>".$row["USERNAME"]."</td><td>".$row["NOME"]."</td><td>".$row["COGNOME"]."</td><td><button><a href=\"DeleteUtenti.php?id={$row['ID']}\">> X <</a></button></td></tr>";
            }
}

public function materialeAdmin(){

  $clauses=array();

  if(isset($_POST['search']))
  {

      if( isset( $_POST['f_tipo'] ) && !empty( $_POST['f_tipo'] ) ){
          $clauses[] = "TIPO LIKE '%{$_POST['f_tipo']}%'";
      }
      if( isset( $_POST['f_descrizione'] ) && !empty( $_POST['f_descrizione'] ) ){
          $clauses[] = "DESCRIZIONE LIKE '%{$_POST['f_descrizione']}%'";
      }
      if ( isset( $_POST['f_proprietario'] ) && !empty( $_POST['f_proprietario'] ) ){
          $clauses[]="PROPRIETARIO LIKE '%{$_POST['f_proprietario']}%'";
      }

      $where = !empty( $clauses ) ? ' where '.implode(' and ',$clauses ) : '';
      $sql = "SELECT ID, TIPO, DESCRIZIONE, PROPRIETARIO FROM testa_risorse " . $where;
      $result = $this->PDO->query($sql);
}
else {
  $sql = "SELECT ID, TIPO, DESCRIZIONE, PROPRIETARIO FROM testa_risorse ";
  $result = $this->PDO->query($sql);
}
      while($row=$result->fetch()){
        echo "<tr><td>".$row["TIPO"]."</td><td>".$row["DESCRIZIONE"]."</td><td>".$row["PROPRIETARIO"]."</td><td><button><a href=\"Dettaglio.php?id={$row['ID']}\">Dettaglio</a></button></td><td><button><a href=\"DeleteMateriale.php?id={$row['ID']}\">> X <</a></button></td></tr>";
            }
      }



public function deleteUtenti(){
  $id_utente = $_GET['id'];
  $query_destroy="DELETE users from users where ID = $id_utente";
  $result = $this->PDO->query($query_destroy);
}

public function deleteMateriale(){
  $id_materiale = $_GET['id'];
  $query_destroy="DELETE testa_risorse from testa_risorse where ID = $id_materiale";
  $result = $this->PDO->query($query_destroy);
}

public function adminNuovoMateriale($post){
  $input_tipo = trim($post['TIPO']);
  $input_proprietario = trim($post['PROPRIETARIO']);
  $input_descrizione = trim($post['DESCRIZIONE']);
  $input_dettaglio = trim($post['DETTAGLIO']);

  try {
    $q= "INSERT INTO testa_risorse (TIPO,PROPRIETARIO,DESCRIZIONE,DETTAGLIO) VALUES(:tipo, :propietario, :descrizione, :dettaglio)";
    $rq = $this->PDO->prepare($q);
    $rq->bindParam(":tipo", $input_tipo, PDO::PARAM_STR);
    $rq->bindParam(":propietario", $input_proprietario, PDO::PARAM_STR);
    $rq->bindParam(":descrizione", $input_descrizione, PDO::PARAM_STR);
    $rq->bindParam(":dettaglio", $input_dettaglio, PDO::PARAM_STR);
    $rq->execute();
  }
  catch (PDOException $e) {
    echo "Errore durante l'Inserimento Dati";
  }

header('location:MaterialeGestione.php');

}

}




 ?>
