<?php
  require '../SITO/PHP/AuthSystem/default.php';
  if($_POST){
    try {
      $auth->registraNuovoUtente($_POST);
    } catch (Exception $e) {
      echo $e -> getMessage();

    }
  }
 ?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8">

    <title>UniLife - Dicci la tua</title>
    <meta name="keywords" content="univeristà di torino, UniTo, CPS, ICT, materiale didattico, didattica, aula studio, aule studio, studenti, studente">
    <meta name="description" content="Aiuto allo Studio - UniLife - Studenti a Torino">
    <meta name="author" content="CORSETO KEVIN">


    <!-- my style -->
   <link href="../css/style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  </head>
  <body>

        <header>
            </header>

            <nav>
                <ul class="topnav">
                  <li><a href="index.html">Homepage</a></li>
                  <li><a href="Torino.html">Perché Torino?</a></li>
                  <li><a href="OrariAuleStudio.html">Aule</a></li>
                  <li><a href="aiutostudio.html">Aiuto allo Studio</a></li>
                  <li><a href="location.html">Visitare e Svago</a></li>
                  <li><a href="form.html">Dicci la tua</a></li>
                  <li id="login"><a href="register.php">Registrati</a></li>

                </ul>
            </nav>




                    <div class="cardPanel"><a href="index.html">

                        <div class="cardHeader">
                            <a href="index.html" ><img src="../IMMAGINI/UL_Mark.svg.png" alt="logo" class="logoImg"></a>
                        </div>
                        <div class="cardContainer">
                            <h1><strong>Uni</strong>Life - Studenti a Torino</h1>

                        </div></a>
                    </div>

                    <div class="section">
                      <h2>Registrazione</h2>
                      </div>

                    <div class="section">


                    <form class="" action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
                      <fieldset>
                    <!-- CASELLE DI TESTO -->

                    <input type="text" class="form_control" name="USERNAME" placeholder="USERNAME"><br>


                    <input type="text" class="form_control" name="NOME" placeholder="NOME"><br>


                    <input type="text" class="form_control" name="COGNOME" placeholder="COGNOME"><br>


                    <input type="password" class="form_control" name="PASSWORD" placeholder="PASSWORD"><br>


                    <input type="password" class="form_control" name="CONFERMA_PASSWORD" placeholder="CONFERMA PASSWORD"><br>



                    <!-- SUBMIT -->
                    <label for="REGISTER"><input class="input" type="submit" name="invia" value="REGISTER"></label><br>

                    <h3>Sei già registrato?</h3>
                    <a href="login.php">ACCEDI</a>




<br><br>
</fieldset><br><br>
                    </form>
</div>

</html>
